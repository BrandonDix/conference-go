import requests
import json
from events.keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


# location
def get_location_image(city: str):
    url = "https://api.pexels.com/v1/search"
    payload = {"query": city, "total_results": 1, "per_page": 1}
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.request("GET", url, params=payload, headers=headers)
    image_dictionary = json.loads(response.content)
    image_url = image_dictionary["photos"][0]["url"]
    return image_url


# conference
def get_lat_lon(city: str) -> dict:
    url = "http://api.openweathermap.org/geo/1.0/direct"
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    payload = {"q": city, "limit": 5, "appid": OPEN_WEATHER_API_KEY}
    response = requests.request("GET", url, params=payload, headers=headers)
    lat_lon_data = json.loads(response.content)
    lat_lon_dic = {
        "lat": lat_lon_data[0]["lat"],
        "lon": lat_lon_data[0]["lon"],
    }
    return lat_lon_dic


def get_weather_data(lat_lon_dic):
    url = "http://api.openweathermap.org/data/2.5/weather"
    payload = {
        "lat": lat_lon_dic["lat"],
        "lon": lat_lon_dic["lon"],
        "units": "imperial",
        "APPID": OPEN_WEATHER_API_KEY,
    }
    response = requests.request("GET", url, params=payload)
    weather_data = json.loads(response.content)
    weather = {
        "temp": weather_data["main"]["temp"],
        "description": weather_data["weather"][0]["description"],
    }
    return weather


def get_weather(city: str):
    lat_lon_dic = get_lat_lon(city)
    results = get_weather_data(lat_lon_dic)
    return results
